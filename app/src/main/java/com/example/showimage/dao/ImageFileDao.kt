package com.example.showimage.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.showimage.bean.ImageFile
import kotlinx.coroutines.flow.Flow

@Dao
interface ImageFileDao {

    @Insert
    suspend fun insert(imageFile: ImageFile)

    @Update
    suspend fun update(imageFile: ImageFile)

    @Query("DELETE FROM image_files")
    suspend fun deleteAll()

    @Query("SELECT * FROM image_files")
    suspend fun getAll(): List<ImageFile>

    @Query("SELECT DISTINCT filepath FROM image_files")
    suspend fun getNames(): List<String>

    @Query("SELECT DISTINCT filename FROM image_files WHERE filepath = :filePath")
    suspend fun getTypeNames(filePath: String): List<String>

    @Query("SELECT * FROM image_files WHERE filename = :filename")
    suspend fun getPicturePath(filename: String): List<ImageFile>

    @Query("SELECT * FROM image_files WHERE filename = :filename LIMIT :limit OFFSET :offset")
    suspend fun getOrderPicturePath(filename: String, limit: Int, offset: Int): List<ImageFile>
}