package com.example.showimage.fragment

import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.example.showimage.MainActivity
import com.example.showimage.adapter.TypeRecyclerAdapter
import com.example.showimage.databinding.TypeListFragmentBinding
import com.example.showimage.db.AppDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TypeListFragment : Fragment() {

    private lateinit var listFragmentBinding: TypeListFragmentBinding

    private var namelist: MutableList<String> = mutableListOf<String>()

    private var recyclerAdapter: TypeRecyclerAdapter? = null

    private var h: Handler? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        listFragmentBinding = TypeListFragmentBinding.inflate(inflater)
        iniView()
        return listFragmentBinding.root
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            iniView()
        }
    }


    fun iniView(){
        arguments?.let {
            it.getString("name")?.let {
                lifecycleScope.launch(Dispatchers.Main) {
                    withContext(Dispatchers.IO) {
                        AppDatabase.getDatabase(requireActivity()).imageFileDao().getTypeNames(it)
                            .toMutableList().also { namelist = it }
                    }
                    h = (requireActivity() as MainActivity).H
                    recyclerAdapter = TypeRecyclerAdapter()
                    listFragmentBinding.recyclerView.adapter = recyclerAdapter
                    recyclerAdapter?.setData(namelist)
                    recyclerAdapter?.setRecyclerViewItemOnClickListener(object:
                        TypeRecyclerAdapter.RecyclerViewItemOnClickListener {
                        override fun setRecyclerItemOnClickListener(position: Int) {
                            Log.d("TAG-sun", "setRecyclerItemOnClickListener: ")
                            Message.obtain(h,2,namelist[position]+"|"+arguments?.getString("name"))
                                .sendToTarget()
                        }
                    })
                }
            }
        }
    }

}