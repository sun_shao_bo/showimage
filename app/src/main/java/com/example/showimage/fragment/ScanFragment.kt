package com.example.showimage.fragment

import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.lifecycle.lifecycleScope
import com.example.showimage.base.BaseFragment
import com.example.showimage.databinding.ScanFragmentBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class ScanFragment : BaseFragment<ScanFragmentBinding>() {
    override fun initView() {
        lifecycleScope.launch(Dispatchers.Main) {
            binding?.let {
                val webSettings: WebSettings = it.web.getSettings()
                //如果访问的页面中要与Javascript交互，则webview必须设置支持Javascript
                webSettings.javaScriptEnabled = true
                //设置自适应屏幕，两者合用
                webSettings.useWideViewPort = true //将图片调整到适合webview的大小
                webSettings.loadWithOverviewMode = true // 缩放至屏幕的大小
                webSettings.layoutAlgorithm = WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING //支持内容重新布局
                webSettings.textZoom = 2 //设置文本的缩放倍数，默认为 100
                webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH) //提高渲染的优先级
                webSettings.standardFontFamily = "" //设置 WebView 的字体，默认字体为 "sans-serif"
                webSettings.defaultFontSize = 20 //设置 WebView 字体的大小，默认大小为 16
                webSettings.minimumFontSize = 12 //设置 WebView 支持的最小字体大小，默认为 8
                it.web.loadUrl("https://www.javbus.com/")
                //系统默认会通过手机浏览器打开网页，为了能够直接通过WebView显示网页，则必须设置
                it.web.getSettings().setJavaScriptEnabled(true)
                it.web.setWebViewClient(object : WebViewClient() {
                    @Deprecated("Deprecated in Java")
                    override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                        //使用WebView加载显示url
                        view.loadUrl(url)
                        return true
                    }

                    override fun onPageFinished(view: WebView?, url: String?) {
                        super.onPageFinished(view, url)
                        // 在这里执行 JavaScript 代码来移除 class 为 "ad-box all-top" 的所有模块
                        view?.loadUrl("javascript:(function() { " +
                                "var elements = document.getElementsByClassName('ad-box all-top'); " +
                                "while (elements.length > 0) { " +
                                "elements[0].parentNode.removeChild(elements[0]); " +
                                "} " +
                                "})();")
                        view?.loadUrl("javascript:(function() { " +
                                "var elements = document.getElementsByClassName('ad-box'); " +
                                "while (elements.length > 0) { " +
                                "elements[0].parentNode.removeChild(elements[0]); " +
                                "} " +
                                "})();")
                    }
                })
            }
        }
    }
}