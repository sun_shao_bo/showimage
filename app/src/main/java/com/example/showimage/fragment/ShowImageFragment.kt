package com.example.showimage.fragment

import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnScrollListener
import com.example.showimage.MainActivity
import com.example.showimage.adapter.ImageAdapter
import com.example.showimage.bean.ImageFile
import com.example.showimage.databinding.PagerFragmentBinding
import com.example.showimage.db.AppDatabase
import com.example.showimage.listener.RecyclerViewScrollListener
import com.example.showimage.view.PageMoveView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class ShowImageFragment : Fragment() {

    private lateinit var pagerFragmentBinding: PagerFragmentBinding

    private var list: MutableList<ImageFile> = mutableListOf<ImageFile>()

    private var h: Handler? = null

    private var i = 0

    private var imageAdapter: ImageAdapter? = null

    private var recyclerViewScrollListener: RecyclerViewScrollListener = RecyclerViewScrollListener()

    private var needUpdateData = true

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        pagerFragmentBinding = PagerFragmentBinding.inflate(inflater)
        initView()
        return pagerFragmentBinding.root
    }


    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!hidden) {
            initView()
        }
    }


    fun initView() {
        lifecycleScope.launch(Dispatchers.Main) {
            withContext(Dispatchers.IO) {
                arguments?.getString("type")?.let {
                    AppDatabase.getDatabase(requireActivity()).imageFileDao()
                        .getPicturePath(it)
                        .toMutableList().let { list = it }
                }
            }
            h = (requireActivity() as MainActivity).H
            Log.d("TAG-sun", "backFragment ${list.size}")
            pagerFragmentBinding.page.setData(list)
            pagerFragmentBinding.page.setOnBackListener(object: PageMoveView.BackListener{
                override fun backFragment() {
                    Log.d("TAG-sun", "backFragment ")
                    Message.obtain(h,1,arguments?.getString("name")).sendToTarget()
                }
            })
            imageAdapter = ImageAdapter(requireContext(), list)

            pagerFragmentBinding.list.adapter = imageAdapter
            recyclerViewScrollListener.setOnScrollDownListener {
                if (needUpdateData) {
                    needUpdateData = false
                    Log.d("TAG-sun", "needUpdateData 前: $needUpdateData")
                    lifecycleScope.launch(Dispatchers.Main) {
                        //加载最后一个
                        if (list.size == 10) {
                            imageAdapter!!.notifyItemChangedPosition(9,1)
                        }
                        delay(2000)
                        withContext(Dispatchers.IO) {
                            i = i + 1
                            arguments?.getString("type")?.let {
                                AppDatabase.getDatabase(requireActivity()).imageFileDao()
                                    .getOrderPicturePath(it, 10, 10 * i)
                                    .toMutableList().let {
                                        list = it

                                    }
                            }
                        }
                        Log.d("TAG-sun", "needUpdateData 后: $needUpdateData")
                        if (list.size > 0) {
                            imageAdapter!!.setData(list)
                            pagerFragmentBinding.list.scrollToPosition(0)
                        }
                        needUpdateData = true
                    }
                }
                false
            }
            recyclerViewScrollListener.setOnScrollUpListener {
                if (needUpdateData) {
                    needUpdateData = false
                    lifecycleScope.launch(Dispatchers.Main) {
                        //加载最后一个
                        imageAdapter!!.notifyItemChangedPosition(0,-1)
                        delay(2000)
                        if(i<=0){
                          return@launch
                        }
                        withContext(Dispatchers.IO) {
                            i = i - 1
                            arguments?.getString("type")?.let {
                                AppDatabase.getDatabase(requireActivity()).imageFileDao()
                                    .getOrderPicturePath(it, 10, 10 * i)
                                    .toMutableList().let {
                                        list = it

                                    }
                            }
                        }
                        Log.d("TAG-sun", "needUpdateData 后: $needUpdateData")
                        if (list.size > 0) {
                            imageAdapter!!.setData(list)
                            pagerFragmentBinding.list.scrollToPosition(0)
                        }
                        needUpdateData = true
                    }
                }
                true
            }
            pagerFragmentBinding.list.addOnScrollListener(recyclerViewScrollListener)
        }
    }



}