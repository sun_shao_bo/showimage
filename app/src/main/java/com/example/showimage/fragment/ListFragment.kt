package com.example.showimage.fragment

import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.showimage.MainActivity
import com.example.showimage.adapter.RecyclerAdapter
import com.example.showimage.base.BaseFragment
import com.example.showimage.bean.ImageFile
import com.example.showimage.databinding.ListFragmentBinding
import com.example.showimage.db.AppDatabase
import com.example.showimage.mvvm.ListViewModel
import com.example.showimage.mvvm.WebViewModel
import com.example.showimage.utils.Contact.IMAGE_FRAGMENT
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class ListFragment: BaseFragment<ListFragmentBinding>() {

    private var namelist: MutableList<String> = mutableListOf<String>()

    private var recyclerAdapter:RecyclerAdapter? =null

    private val viewModel: ListViewModel by viewModels()

    override fun initView(){

        lifecycleScope.launch(Dispatchers.Main) {
            withContext(Dispatchers.IO) {
                viewModel.getNameList()
                recyclerAdapter = RecyclerAdapter()
            }
            viewModel._namelistFlow.collect{
                namelist = it.toMutableList()
                binding?.recyclerView?.adapter = recyclerAdapter
                recyclerAdapter?.setData(namelist)
                recyclerAdapter?.setRecyclerViewItemOnClickListener(object: RecyclerAdapter.RecyclerViewItemOnClickListener {
                    override fun setRecyclerItemOnClickListener(position: Int) {
                        Message.obtain(h,IMAGE_FRAGMENT,namelist[position]).sendToTarget()
                    }
                })
            }
        }
    }
    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if(!hidden){
            initView()
        }
    }

}