package com.example.showimage.fragment


import android.util.Log
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.showimage.base.BaseFragment
import com.example.showimage.base.Constant
import com.example.showimage.databinding.WebFragmentBinding
import com.example.showimage.mvvm.WebViewModel
import com.example.showimage.modul.TJsoupUtil
import com.example.showimage.utils.AlertDialogUtils
import com.example.showimage.view.WebViewPlus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.jsoup.nodes.Document


class WebFragment : BaseFragment<WebFragmentBinding>() {

    // 使用 viewModels 扩展属性获取 ViewModel 实例
    private val viewModel: WebViewModel by viewModels()

    override fun initView() {
        initWebView()
        initListener()
    }

    private fun initListener() {
        // view事件监听
        binding?.let {
            viewModel.setUrl(Constant.IMAGE_URL)
            //下载
            it.download.setOnClickListener {
                viewModel.setShowDialog()
            }
            //加载上一个页面
            it.prePage.setOnClickListener {
                binding?.web?.goBack()
                viewModel.loadPreUrl()
            }
            it.web.setWebViewClient(object : WebViewClient() {
                override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                    viewModel.setUrl(url)
                    return true
                }
            })
            it.web.setScrollChanged(object : WebViewPlus.ScrollChanged {
                override fun scrollChange() {
                    viewModel.setController()
                }
            })
        }
        //控制栏发生改变
        viewModel.getController().observe(requireActivity()) {
            if (it) {
                binding?.controller?.visibility = View.VISIBLE
            } else {
                binding?.controller?.visibility = View.GONE
            }
        }
        // 刷新URL
        viewModel.getUrlLiveData().observe(requireActivity()) {
            binding?.web?.loadUrl(it)
        }
        // 显示弹窗
        viewModel.getShowDialog().observe(requireActivity()) {
            if (it) {
                showCustomDialog(viewModel.getUrlLiveData().value.toString())
            }
        }
        // 显示下载图标
        viewModel.imageDownLoadVisibleLiveData().observe(requireActivity()) {
            if(it){
                binding?.imageLoad?.visibility = View.VISIBLE
                binding?.controller?.visibility = View.VISIBLE
            }else{
                binding?.imageLoad?.visibility = View.GONE
                binding?.controller?.visibility = View.GONE
            }
        }
        // 显示下载图标
        viewModel.imageDownLoadProgressVisibleLiveData().observe(requireActivity()) {
            binding?.imageLoad?.text = it
        }
    }

    private fun initWebView() {
        val webSettings: WebSettings = binding!!.web.getSettings()
        webSettings.javaScriptEnabled = true
        webSettings.useWideViewPort = true
        webSettings.loadWithOverviewMode = true
        webSettings.layoutAlgorithm = WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING
        webSettings.textZoom = 2
        webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH)
        webSettings.standardFontFamily = ""
        webSettings.defaultFontSize = 20
        webSettings.minimumFontSize = 12
    }

    override fun onStop() {
        super.onStop()
        binding?.web?.stopLoading()
    }


    private fun showCustomDialog(msg: String) {
        var document:Document? = null
        var name = ""
        var title = ""
        lifecycleScope.launch(Dispatchers.Main) {
            try {
                withContext(Dispatchers.IO) {
                    document = TJsoupUtil.findDocument(msg)
                    title = TJsoupUtil.findName(document!!)
                    name = title.split("期")[1].let {
                        it.split("写真")[0]
                    }
                }
            }catch (e: Exception){
                Log.d("WebFragment", "showCustomDialog: ")
            }
            document?.let {
                AlertDialogUtils.createAlertDialog(requireActivity(), it, name,title)
            }
        }
    }
}