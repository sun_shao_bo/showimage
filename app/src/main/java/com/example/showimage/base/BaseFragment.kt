package com.example.showimage.base

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.example.showimage.MainActivity
import com.example.showimage.service.DownloadFileManager
import com.example.showimage.service.DownloadService
import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type


open abstract class BaseFragment<T : ViewBinding>() : Fragment() {

    protected var binding: T? = null

    private var binder: DownloadService.DownloadBinder? = null

    protected var h: Handler? =null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val superclass: Type = javaClass.genericSuperclass
        val aClass =
            (superclass as ParameterizedType).actualTypeArguments[0] as Class<*>
        try {
            val method: Method = aClass.getDeclaredMethod(
                "inflate",
                LayoutInflater::class.java,
                ViewGroup::class.java,
                Boolean::class.javaPrimitiveType
            )
            binding = method(null, layoutInflater, container, false) as T
            binder = DownloadFileManager.getInstance().getBinder()
            h = (requireActivity() as MainActivity).H
            initView()
        } catch (e: NoSuchMethodException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        } catch (e: InvocationTargetException) {
            e.printStackTrace()
        }
        return binding!!.root
    }

    abstract fun initView()
}




