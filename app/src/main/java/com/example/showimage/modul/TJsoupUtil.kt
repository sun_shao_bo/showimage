package com.example.showimage.modul

import android.util.Log
import com.example.showimage.bean.ImageFile
import com.example.showimage.utils.JsoupUtils
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.select.Elements


class TJsoupUtil : JsoupUtils() {

    companion object {

        suspend fun findDocument(url: String): Document {
            val picturepage = Jsoup.connect(url).userAgent(
                // add userAgent. TODO There is a plan to configure userAgent to load that userAgent from a property file.
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.30 (KHTML, like Gecko) Chrome/12.0.742.122 Safari/534.30"
            ).timeout(10000).get()
            return Jsoup.parse(picturepage.html())
        }

        // 定义一个方法来查找图片列表
        suspend fun findImagesList(html: Document, name: String,title:String): MutableList<ImageFile> {
            var list = mutableListOf<ImageFile>()
            // 使用选择器语法获取所有带有 class="wp-block-image size-full" 的 figure 标签
            val figures: Elements = html.select("figure.wp-block-image.size-full")
            if (figures == null) {
                return list
            }
            figures.forEachIndexed { index, element ->
                // 获取 figure 标签中的 img 标签
                val imgs = element.select("img")
                if (imgs[0] != null) {
                    val img = imgs[0]
                    // 获取 img 标签的 src 属性值
                    val href = img.attr("src")
                    list.add(ImageFile(name, href, title, "${index}.jpg"))
                } else {
                    println("没有找到 src 属性")
                }
            }
            return list
        }

        suspend fun findName(doc: Document): String {
            // 使用选择器语法获取具有 class 为 item_title 的元素
            val elements = doc.select(".item_title")
            // 遍历并打印每个元素的内容
            for (element in elements) {
                if (element.select("h1") != null) {
                    return element.select("h1").text()
                }
            }
            return ""
        }
    }
}
