package com.example.showimage

import android.app.Application
import android.app.DownloadManager
import android.content.Context
import com.example.showimage.permession.PermessionManager
import com.example.showimage.service.DownloadFileManager

class App:Application() {

    companion object {
        lateinit var instance: Application
    }

    init {
        instance = this
    }

    override fun onCreate() {
        super.onCreate()
        //application全局的应用
        PermessionManager.init(this)
        DownloadFileManager.bind(this)
    }


}