package com.example.showimage.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import com.example.showimage.databinding.RecyclerviewItemBinding
import com.kt.android.base.BaseRecyclerViewHolder
import com.kt.android.base.IBaseAdapter



class RecyclerAdapter() : IBaseAdapter<String, ViewBinding>() {

    var itemOnClickListener:RecyclerViewItemOnClickListener? = null

    class VH(binding: ViewBinding) :
        BaseRecyclerViewHolder<String, ViewBinding>(binding as RecyclerviewItemBinding) {
        override fun bind(t: String) {
            (binding as RecyclerviewItemBinding).name.text = t
        }
    }

    override fun createViewHolder(binding: ViewBinding, viewType: Int): BaseRecyclerViewHolder<String, ViewBinding> {
        return  VH(binding)
    }

    override fun createBind(
        layoutInflater: LayoutInflater?,
        parent: ViewGroup,
        viewType: Int
    ): ViewBinding {
        return RecyclerviewItemBinding.inflate(layoutInflater!!, parent, false)
    }

    override fun onBindViewHolder(
        holder: BaseRecyclerViewHolder<String, ViewBinding>,
        position: Int
    ) {
        val student = list?.get(position)
        holder.itemView.setOnClickListener {
            Log.d("TAG-sun", "onBindViewHolder: ")
            itemOnClickListener?.setRecyclerItemOnClickListener(position)
        }
        holder.bind(student!!)
    }

    fun setRecyclerViewItemOnClickListener(recyclerViewItemOnClickListener:RecyclerViewItemOnClickListener){
        this.itemOnClickListener = recyclerViewItemOnClickListener
    }

    interface RecyclerViewItemOnClickListener{
        fun setRecyclerItemOnClickListener(position: Int)
    }


    override fun getItemCount(): Int {
        return super.getItemCount()
    }


}