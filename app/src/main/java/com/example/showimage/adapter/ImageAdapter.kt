package com.example.showimage.adapter

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Environment
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.showimage.R
import com.example.showimage.bean.ImageFile
import com.example.showimage.utils.Contact
import java.io.File

class ImageAdapter(private val context: Context, private var items: MutableList<ImageFile>) :
    RecyclerView.Adapter<ImageAdapter.ViewHolder>() {

    private var status = 0


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (getItemViewType(position) == Contact.IMAGE_ITEM_TYPE) {
            val item = items[position]
            val dir = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
            val file = File(dir, "${item.filepath}/${item.filename}/${item.index}")
            Log.d("TAG-sun", "onBindViewHolder: ${(holder as ViewHolder).imageView}")
            val layoutParams = holder.itemView.layoutParams
            layoutParams.height = getScreenHeight()
            (holder as ViewHolder).itemView.layoutParams = layoutParams
            // 使用 Glide 加载图片
            Glide.with(context)
                .load(file)
                .into(holder.imageView)
            holder.load.visibility = View.GONE
            if (position == 9 && status == 1) {
                holder.load.visibility = View.VISIBLE
                Glide.with(context)
                    .load(R.drawable.load)
                    .into(holder.load)
                startRotationAnimation(holder.load)
            }
            if (position == 0 && status == -1) {
                holder.load.visibility = View.VISIBLE
                Glide.with(context)
                    .load(R.drawable.load)
                    .into(holder.load)
                startRotationAnimation(holder.load)
            }
        } else {

        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageView: ImageView = itemView.findViewById(R.id.imageview)
        val load: ImageView = itemView.findViewById(R.id.load)
    }

    fun setData(datas: MutableList<ImageFile>) {
        items.clear()
        items.addAll(datas)
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return Contact.IMAGE_ITEM_TYPE

    }

    fun startRotationAnimation(imageView: ImageView) {
        val animator = ObjectAnimator.ofFloat(imageView, "rotation", 0f, 130f,270f)
        animator.repeatCount = ObjectAnimator.INFINITE // 无限循环
        animator.interpolator = AccelerateInterpolator()
        val animatorAlpha = ObjectAnimator.ofFloat(imageView, "alpha", 0f, 1f)
        val animatorY = ObjectAnimator.ofFloat(imageView, "translationY", 0f,if(status==1){getScreenHeight()-80f}else{30f})
        var animatorSet = AnimatorSet()
        animatorSet.playTogether(animator, animatorY,animatorAlpha)
        animatorSet.duration = 500
        animatorSet.start()
        status = 0
    }

    fun notifyItemChangedPosition(position: Int,s: Int) {
        if (items.size == 10) {
            status = s
            notifyItemChanged(position)
        }
    }

    fun getScreenHeight():Int{
        return context.resources.displayMetrics.heightPixels
    }

}