package com.example.showimage.listener

import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager

open class RecyclerViewScrollListener : RecyclerView.OnScrollListener() {

    private var canScrollVerticallyUp: () -> Boolean = { true }

    private var canScrollVerticallyDown: () -> Boolean = { true }

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        if (dy > 0) { // 向下滚动
            if (!recyclerView.canScrollVertically(1)) {
                canScrollVerticallyDown.invoke()
            }
        } else if (dy < 0) { // 向上滚动
            if (!recyclerView.canScrollVertically(-1)) {
                canScrollVerticallyUp.invoke()
            }
        }
    }

    open fun setOnScrollUpListener(l: () -> Boolean) {
        canScrollVerticallyUp = l
    }

    open fun setOnScrollDownListener(l: () -> Boolean) {
        canScrollVerticallyDown = l
    }
}