package com.example.showimage


import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.util.Log
import android.view.WindowManager
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.showimage.base.BaseActivity
import com.example.showimage.bean.ImageFile
import com.example.showimage.databinding.ActivityMainBinding
import com.example.showimage.fragment.ListFragment
import com.example.showimage.fragment.ScanFragment
import com.example.showimage.fragment.ShowImageFragment
import com.example.showimage.fragment.TypeListFragment
import com.example.showimage.fragment.WebFragment
import com.example.showimage.utils.Contact
import com.example.showimage.utils.Contact.IMAGE_FRAGMENT


class MainActivity : BaseActivity<ActivityMainBinding>() {

    var list: MutableList<ImageFile> = mutableListOf<ImageFile>()

    var currentFragment: Fragment? = null

    var fragmentList = arrayListOf<Fragment>(
        ListFragment(),
        WebFragment(),
        TypeListFragment(),
        ShowImageFragment(),
        ScanFragment()
    ).toMutableList()

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkPermissions()
        initView()
    }


    private fun initView() {
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        // 实例化适配器并设置给ViewPager2
        binding.navView.setOnItemSelectedListener { item ->
            when (item.title) {
                "列表" -> {
                    switchFragment(fragmentList[0], "list")
                }
                "网页" -> {
                    switchFragment(fragmentList[1], "web")
                }
                "设置" -> {
                    switchFragment(fragmentList[4], "设置")
                }
                else -> {}
            }
            true
        }
        switchFragment(fragmentList[0], "list")
    }

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    private fun checkPermissions() {
        Contact.PERMISSIONS.forEach {
            if (ContextCompat.checkSelfPermission(this, it) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(
                    this,
                    Contact.PERMISSIONS,
                    Contact.PERMISSION_REQUEST_CODE
                )
                return
            }
        }
        onBackPressedDispatcher.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                Log.d("TAG-sun", "handleOnBackPressed: ")
            }
        })
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            Contact.PERMISSION_REQUEST_CODE -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("sun_bo", "checkPermissions: 权限有了")
                }
            }
        }
    }


    fun switchFragment(fragment: Fragment, tag: String) {
        val fragmentManager: FragmentManager = supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        Log.d("TAG-sun", "switchFragment: $fragment")
        if (!fragment.isAdded() && null == fragmentManager.findFragmentByTag(tag)) {
            if (currentFragment != null) {
                currentFragment?.let {
                    fragmentTransaction.hide(it)
                }
            }
            fragmentTransaction
                .add(R.id.fragment, fragment, tag)
                .commit()
        } else {
            fragmentList.forEach {
                if (!it.isHidden)
                    fragmentTransaction.hide(it)
            }
            fragmentTransaction
                .show(fragment)
                .commit()
        }
        currentFragment = fragment
    }

    var H: Handler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            val bundle = Bundle()
            when (msg.what) {
                IMAGE_FRAGMENT -> {
                    val name = msg.obj as String
                    bundle.putString("name", name)
                    switchFragment(fragmentList[2].apply { arguments = bundle }, "type")
                }
                2 -> {
                    val name = msg.obj as String
                    val names = name.split("|")
                    bundle.putString("type", names[0])
                    bundle.putString("name", names[1])
                    switchFragment(fragmentList[3].apply { arguments = bundle }, "show")
                }
            }
        }
    }

}
