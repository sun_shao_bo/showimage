package com.example.showimage.bean

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "image_files")
data class ImageFile(
    @ColumnInfo(name = "filepath") var filepath: String, //名字
    @ColumnInfo(name = "href") var href: String,
    @ColumnInfo(name = "filename") var filename: String,//模块
    @ColumnInfo(name = "index") var index: String,//文件名称
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var id: Long = 0L
)
