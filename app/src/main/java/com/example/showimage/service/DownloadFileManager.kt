package com.example.showimage.service

import android.content.ComponentName
import android.content.Context
import android.content.ServiceConnection
import android.os.IBinder
import android.util.Log

class DownloadFileManager {

    fun getBinder():DownloadService.DownloadBinder?{
        return binder
    }

    companion object{
        private var downloadFileManager: DownloadFileManager? = null


        fun bind(context: Context){
            downloadFileManager = DownloadFileManager()
            val intent = android.content.Intent(context,DownloadService::class.java)
            context.bindService(intent,serviceConnection,Context.BIND_AUTO_CREATE)
        }

        fun getInstance():DownloadFileManager{
            return downloadFileManager!!
        }

        var binder: DownloadService.DownloadBinder? = null

        private var serviceConnection = object : ServiceConnection {

            override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
                binder = service as DownloadService.DownloadBinder
                Log.d("TAG-sun", "onServiceConnected:  连接成功")
                binder?.setDownLoadListener(object : DownloadService.DownloadBinder.DownLoadListener {
                    override fun onDownLoadListener(index: Int, size: Int, name: String) {
                        Log.d("TAG-sun", "onServiceConnected:  index = ${index} size=${size} ")
                    }
                })
            }

            override fun onServiceDisconnected(name: ComponentName?) {
                Log.d("TAG-sun", "onServiceConnected: 断开连接")
            }

        }

    }



}
