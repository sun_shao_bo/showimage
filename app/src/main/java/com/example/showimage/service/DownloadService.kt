package com.example.showimage.service

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.util.Log
import com.example.showimage.db.AppDatabase
import com.example.showimage.modul.TJsoupUtil
import com.example.showimage.utils.JsoupUtils
import com.example.showimage.utils.JsoupUtils.Companion.downloadWebpAndConvertToJpg
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

class DownloadService : Service() {

    override fun onBind(intent: Intent): IBinder {
        return DownloadBinder(this)
    }


    class DownloadBinder(context: Context) : Binder() {

        private var context: Context? = null

        private var downLoadListener: DownLoadListener? = null

        init {
            this.context = context
        }

        fun downloadImage(url: Document, name: String,title:String) {
            MainScope().launch(Dispatchers.IO) {
                var hrefs = TJsoupUtil.findImagesList(url, name,title)
                hrefs.forEachIndexed { index, imageFile ->
                        downloadWebpAndConvertToJpg(imageFile, context!!)
                        AppDatabase.getDatabase(context!!).imageFileDao().insert(imageFile)
                        downLoadListener?.let {
                            it.onDownLoadListener(index+1,hrefs.size,name)
                        }
                    }
            }
        }


        fun setDownLoadListener(downLoadListener: DownLoadListener) {
            this.downLoadListener = downLoadListener
        }

        interface DownLoadListener {
            fun onDownLoadListener(index:Int,size:Int,name:String)
        }

    }
}