package com.example.showimage.utils

import android.Manifest
import android.os.Build
import androidx.annotation.RequiresApi


object Contact {

    val PERMISSION_REQUEST_CODE: Int = 10

    val IMAGE_ITEM_TYPE:Int = 0

    val LOADING_ITEM_TYPE:Int = 1

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    val PERMISSIONS = arrayOf(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
    )

    val IMAGE_FRAGMENT = 1
}