package com.example.showimage.utils

import android.annotation.SuppressLint
import android.content.Context
import android.os.PowerManager
import android.os.PowerManager.WakeLock


@SuppressLint("InvalidWakeLockTag")
class ScreenLighter(context: Context) {

    private var wakeLock: WakeLock?

    init {
        var powerManager = context.getSystemService(Context.POWER_SERVICE) as PowerManager
        wakeLock = powerManager!!.newWakeLock(PowerManager.FULL_WAKE_LOCK, "MyScreenLighterTag")
    }

    fun turnOnScreen() {
        wakeLock?.acquire()
    }

    fun turnOffScreen() {
        wakeLock?.release()
    }
}