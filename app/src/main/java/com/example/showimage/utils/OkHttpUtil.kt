package com.example.showimage.utils

import okhttp3.Call
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import java.io.IOException
import java.util.concurrent.TimeUnit


object OkHttpUtil {
    private var singleton: OkHttpClient? = null
    val instance: OkHttpClient?
        get() {
            if (singleton == null) {
                synchronized(OkHttpUtil::class.java) {
                    if (singleton == null) {
                        singleton = OkHttpClient().newBuilder()
                            .connectTimeout(50000, TimeUnit.MILLISECONDS)
                            .readTimeout(50000, TimeUnit.MILLISECONDS)
                            .addInterceptor(object:Interceptor{
                                override fun intercept(chain: Interceptor.Chain): Response {
                                    val call: Call = chain.call()
                                    for (i in 0..2) { // 最多重联3次
                                        try {
                                            return chain.proceed(chain.request())
                                        } catch (e: IOException) {
                                            if (i >= 2 || !call.isExecuted()) { // 超过重联次数或请求已执行则抛出
                                                throw e
                                            }
                                        }
                                    }
                                    throw IOException("Too many follow-up requests")
                                }

                            }) // 添加重联拦截器
                            .build()
                    }
                }
            }
            return singleton
        }
}