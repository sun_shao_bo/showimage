package com.example.showimage.utils

import android.animation.ValueAnimator
import android.util.Log
import com.bumptech.glide.disklrucache.DiskLruCache.Value

object ValueAnimatorUtils {

    var animaValue = HashMap<String, ValueAnimator>()

    fun getAnimatorValue(key: String): ValueAnimator? {
        if (animaValue[key] == null) {
            animaValue[key] = ValueAnimator.ofFloat().apply {
                duration = 100
            }
        }
        return animaValue[key]
    }
}