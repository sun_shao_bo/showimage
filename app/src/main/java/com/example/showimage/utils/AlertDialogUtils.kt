package com.example.showimage.utils

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.example.showimage.R
import com.example.showimage.service.DownloadFileManager
import com.example.showimage.service.DownloadFileManager.Companion.binder
import org.jsoup.nodes.Document

object AlertDialogUtils {

    fun createAlertDialog(context: Context, document: Document, name:String,title:String){
        // 加载布局文件
        val builder = AlertDialog.Builder(context)
        val inflater = LayoutInflater.from(context).inflate(R.layout.popup_layout, null)
        val show = inflater.findViewById<TextView>(R.id.show)
        val close = inflater.findViewById<Button>(R.id.close)
        val confirm = inflater.findViewById<Button>(R.id.confirm)
        show.text = name
        // 设置对话框的内容视图
        builder.setView(inflater)
        // 显示对话框
        val dialog = builder.create()
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
        close.setOnClickListener {
            dialog.dismiss()
        }
        confirm.setOnClickListener {
            DownloadFileManager.getInstance().getBinder()?.downloadImage(document, name,title)
            dialog.dismiss()
        }
    }

}