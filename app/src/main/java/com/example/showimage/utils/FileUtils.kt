package com.example.showimage.utils

import android.content.Context
import android.graphics.Point
import android.os.Environment
import android.util.Log
import android.view.WindowManager
import cn.hutool.core.io.FileUtil
import java.io.File
import java.io.IOException
import java.io.OutputStreamWriter
import java.nio.charset.StandardCharsets


class FileUtils {
    companion object{

        fun saveFile(context: Context,filename:String,path:String){
            val fileName = "output.txt"
            val dir = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
            val file = File(dir, "/out/$fileName")
            val filepath = createFile(file.parent)
            Log.d("sun_bo", "saveFile: ${filepath}")
            try {
                file?.writeText("Hello, world!", Charsets.UTF_8)
                Log.d("MainActivity", "文件已创建: ${file?.exists()} ${file.path}")
            } catch (e: IOException) {
                Log.e("MainActivity", "创建文件失败: ${e.message}")
            }
        }


        fun createFile(file:String):File{
            val dir = File(file)
            if(!dir.exists()){
                dir.mkdirs()
            }
            return dir
        }

        fun getScreenWidth(context: Context): Int {
               val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
              val display = windowManager.defaultDisplay
              val size = Point()
              display.getSize(size) // API 17 及以上
              return size.x
        }
    }
}