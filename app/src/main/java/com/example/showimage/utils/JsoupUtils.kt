package com.example.showimage.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Environment
import android.util.Log
import com.example.showimage.bean.ImageFile
import com.example.showimage.db.AppDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.Request
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.concurrent.Executors
import java.util.regex.Pattern

open class JsoupUtils {

    companion object {


        suspend fun downloadWebpAndConvertToJpg(
            imageFile: ImageFile,
            context: Context,
        ) {
            val client = OkHttpUtil.instance
            val request = Request.Builder()
                .url(imageFile.href)
                .build()
            client?.newCall(request)?.execute().use { response ->
                if (!response?.isSuccessful!!) throw IOException("Unexpected code $response")
                val webpData = response.body?.bytes()
                if (webpData != null) {
                    try {
                        val bitmap = BitmapFactory.decodeByteArray(webpData, 0, webpData.size)
                        saveBitmapAsJpg(bitmap, imageFile, context)
                    } catch (e: Exception) {

                    }
                }
            }
        }


        // 定义一个方法来获取 href 列表
        fun getHrefList(html: String): Set<String> {
            val doc: Document = Jsoup.parse(html)
            val hrefSet = mutableSetOf<String>()
            // 获取名称
//            val n = name(doc)
            // 使用 CSS 选择器
            val element = doc.selectFirst("#__nuxt > div.page-search > div > div.images-list")
            val links = element?.select("a")
            links?.forEach { link ->
                val href = link.attr("href")
                if ("articles" in href) {
                    hrefSet.add(href)
                }
            }
            return hrefSet
        }

        // 定义一个方法来查找图片列表
        suspend fun findImagesList(html: String, name: String): MutableList<ImageFile> {
            var list = mutableListOf<ImageFile>()
            val doc: Document = Jsoup.parse(html)
            val liElements = doc.select("li[data-fancybox=gallery]")
            if (liElements.isEmpty()) {
                return list
            }
            // 查找具有特定类名的h2元素
            val h2Element = doc.selectFirst("h2.title")
            val textName = h2Element?.text()?.trim() ?: ""
            Log.d("TAG", "findImagesList:$textName ")
            liElements.forEachIndexed { index, element ->
                val href = element.attr("href")
                if (href.isNotEmpty()) {
                    list.add(ImageFile(name, href, textName, "${index}.jpg"))
                } else {
                    println("没有找到 href 属性")
                }
            }
            return list
        }



        suspend fun findName(url:String):String{
            val picturepage = Jsoup.connect(url).timeout(10000).get()
            val doc: Document = Jsoup.parse(picturepage.html())
            val liElements = doc.select("li[data-fancybox=gallery]")
            if (liElements.isEmpty()) {
                return "佚名"
            }
            val h2Element = doc.selectFirst("h2.title")
            val textName = h2Element?.text()?.trim() ?: ""
            return textName
        }

        suspend fun findImagesList(html: String): MutableList<ImageFile> {
            var list = mutableListOf<ImageFile>()
            val doc: Document = Jsoup.parse(html)
            val liElements = doc.select("li[data-fancybox=gallery]")
            if (liElements.isEmpty()) {
                return list
            }
            // 查找具有特定类名的h2元素
            val h2Element = doc.selectFirst("h2.title")
            val textName = h2Element?.text()?.trim() ?: ""
            return list
        }

        fun getN(htmlContent: String): String {
            // 解析HTML文档
            val doc: Document = Jsoup.parse(htmlContent)
            // 使用CSS选择器提取文本
            val resultDiv = doc.selectFirst("#__nuxt .title")
            val spans = resultDiv?.select("span")?.first()?.text()

            // 使用正则表达式匹配汉字
            val pattern = Pattern.compile("[\\u4e00-\\u9fff]+")
            val matcher = spans?.let { pattern.matcher(it) }
            Log.d("TAG-sun", "getN: ${spans}")
            // 查找所有匹配的汉字
//            val matches = mutableListOf<String>()
//            while (matcher!!.find()) {
//                matches.add(matcher.group())
//            }
//            var name = matches.joinToString("").split(" ")
            // 合并所有匹配的汉字
            return /*name[name.size - 1]*/"null"
        }

        fun getName(htmlContent: String): String {
            // 解析HTML文档
            val doc: Document = Jsoup.parse(htmlContent)
            // 使用CSS选择器提取文本
            val resultDiv = doc.selectFirst("#__nuxt .title")
            val spans = resultDiv?.select("span")?.first()?.text()

            // 使用正则表达式匹配汉字
            val pattern = Pattern.compile("[\\u4e00-\\u9fff]+")
            val matcher = spans?.let { pattern.matcher(it) }
            // 查找所有匹配的汉字
            val matches = mutableListOf<String>()
            while (matcher!!.find()) {
                matches.add(matcher.group())
            }
            // 合并所有匹配的汉字
            return matches.joinToString("")
        }


        private suspend fun saveBitmapAsJpg(
            bitmap: Bitmap,
            imageFile: ImageFile,
            context: Context
        ) {
            val dir = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
            val file = File(dir, "/${imageFile.filepath}/${imageFile.filename}/${imageFile.index}")
            file.parent?.let { FileUtils.createFile(it) }
            withContext(Dispatchers.IO) {
                val outputStream = FileOutputStream(file)
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
                outputStream.flush()
                outputStream.close()
                bitmap.recycle()
                Log.d("TAG-sun", "saveBitmapAsJpg:正在下载 ${file.path} ")
                Thread.sleep(100)
            }
        }
    }
}