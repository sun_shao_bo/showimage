package com.example.showimage.permession;

public interface PermissionCallback {
    void permissionGranted();

    void permissionRefused();
}
