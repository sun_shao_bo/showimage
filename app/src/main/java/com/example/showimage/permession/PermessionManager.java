package com.example.showimage.permession;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.ArrayList;
import java.util.Arrays;

public class PermessionManager {

    private static Context context;

    private static ArrayList<PermissionRequest> permissionRequests = new ArrayList<PermissionRequest>();

    public static void init(Context context){
        //application全局的应用
        PermessionManager.context = context;
    }



    //对请求进行处理;
    @RequiresApi(api = Build.VERSION_CODES.M)
    public static void askForPermission(Activity activity, String[] permissions, PermissionCallback permissionCallback) {

        if (permissionCallback == null) {
            //不做处理
            return;
        }
        if (hasPermission(activity, permissions)) {
            //已拥有权限
            permissionCallback.permissionGranted();
            return;
        }
        PermissionRequest permissionRequest = new PermissionRequest(new ArrayList<String>(Arrays.asList(permissions)), permissionCallback);
        permissionRequests.add(permissionRequest);
        //发送请求
        activity.requestPermissions(permissions, permissionRequest.getRequestCode());
    }


    //TODO:权限处理流程;
    public static void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        //方法的参数：requestCode请求编码，permission权限，grantResults权限返回值
        PermissionRequest requestResult = new PermissionRequest(requestCode);
        if (permissionRequests.contains(requestResult)) {
            PermissionRequest permissionRequest =
                    permissionRequests.get(permissionRequests.indexOf(requestResult));
            if (verifyPermissions(grantResults)) {
                //Permission has been granted
                permissionRequest.getPermissionCallback().permissionGranted();
            } else {
                permissionRequest.getPermissionCallback().permissionRefused();
            }
            permissionRequests.remove(requestResult);
        }
        //后面有bug再加
        // refreshMonitoredList();
    }

    //判断是否为true
    private static boolean verifyPermissions(int[] grantResults) {
        for(int result:grantResults){
            if(result!=PackageManager.PERMISSION_GRANTED){
                  return false;
            }
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public static boolean hasPermission(Activity activity, String[] permissions) {
        for (String permission : permissions) {
            //数组中的参数只要有一个权限没有获取就返回false,都获取的情况返回true
            if (activity.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public static boolean shouldShowRequestPermissionRationale(Activity activity, String permissions) {
        return activity.shouldShowRequestPermissionRationale(permissions);
    }

    /**
     * 检查是否具有该权限
     * @param permissionName
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    public static boolean checkPermission(String permissionName) {
        if (context == null) {
            throw new RuntimeException("Before comparing permissions you need to call Nammu.init(context)");
        }
        return PackageManager.PERMISSION_GRANTED == context.checkSelfPermission(permissionName);
    }
}
