package com.example.showimage.view

import android.animation.Animator
import android.animation.ValueAnimator
import android.content.Context
import android.os.Environment
import android.text.method.BaseKeyListener
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.showimage.bean.ImageFile
import com.example.showimage.databinding.MovePageViewBinding
import com.example.showimage.utils.FileUtils
import com.example.showimage.utils.ValueAnimatorUtils
import java.io.File
import kotlin.math.abs

class PageMoveView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0)
    : FrameLayout(context,attrs,defStyleAttr) {

    var current = 0

    var startX = 0f

    var startY = 0f

    var transitionX: ValueAnimator? = null

    var transitionView: View? = null

    //1代表下一页 -1代表上一页
    var status: Int = 1

    var datas: MutableList<ImageFile> = mutableListOf<ImageFile>()

    private var screenWidth: Int? = null

    private var imageFile: ImageFile? = null

    private var interceptorUpdate = true

    private var interceptorMove = false

    private var backListener:BackListener? = null


    private var binding: MovePageViewBinding? = null

    init {
        screenWidth = FileUtils.getScreenWidth(context)
        binding = MovePageViewBinding.inflate(LayoutInflater.from(context))
        //binding.root.findViewById<>()
        val viewGroup = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        addView(binding!!.root, viewGroup)
    }


    fun setData(datas: MutableList<ImageFile>) {
        Log.d("TAG-sun", "setData: ${datas}")
        this.datas = datas
        initView()
    }

    private fun initView() {
        transitionX = ValueAnimatorUtils.getAnimatorValue("transitionX")?.apply {
            addUpdateListener {
                transitionView?.translationX = it.animatedValue as Float
            }
            addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {

                }

                override fun onAnimationEnd(animation: Animator) {
                    updateImage(imageFile, binding?.image)
                    interceptorUpdate = true
                    transitionView?.alpha = 0f
                    transitionView?.translationX = 0f
                    transitionView?.alpha = 1f
                }

                override fun onAnimationCancel(animation: Animator) {

                }

                override fun onAnimationRepeat(animation: Animator) {

                }

            })
        }
        if (datas.size <= 0) {
            Log.d("TAG-sun", "initView: 无数据")
        } else {
            updateImage(datas[current], binding?.image)
            updateImage(datas[current], binding?.imageLoad)
            transitionView = binding?.image
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                startX = event.x
                startY = event.y
            }

            MotionEvent.ACTION_MOVE -> {
                //移动距离大于150px开始根手移动
                if (Math.abs(event.x - startX) > 150) {
                    interceptorMove = true
                    //上一页
                    if (event.x > startX) {
                        Log.d("TAG-sun", "onTouchEvent: 上一页")
                        moveImage(-1)
                        transitionView?.translationX = event.x - startX
                    }
                    //下一页
                    if (event.x < startX) {
                        moveImage(1)
                        transitionView?.translationX = event.x - startX
                    }
                } else {
                    interceptorMove = false
                }

            }

            MotionEvent.ACTION_UP -> {
                if (interceptorMove) {
                    transitionX?.setFloatValues(
                        transitionView!!.translationX,
                        -screenWidth!!.toFloat() * status
                    )
                    transitionX?.start()
                }
                if (Math.abs(event.y - startY) > screenWidth!!*2/3) {
                    backListener?.backFragment()
                }
            }

            MotionEvent.ACTION_CANCEL -> {
                transitionX?.setFloatValues(
                    transitionView!!.translationX,
                    -screenWidth!!.toFloat() * status
                )
                transitionX?.start()
                if (Math.abs(event.y - startY) > screenWidth!!*2/3) {
                    backListener?.backFragment()
                }
            }
        }
        return super.onTouchEvent(event)
    }


    private fun moveImage(status: Int) {
        this.status = status
        if (current >= 0 && datas.size > 0) {
            //每次近来北京还一次锁死
            if (interceptorUpdate) {
                if (status == 1) {
                    //下一页
                    current = (current + 1) % datas.size
                } else {
                    if (current == 0) {
                        current = datas.size - 1
                    } else {
                        current -= 1
                    }
                }
                imageFile = datas[current]
                updateImage(imageFile, binding?.imageLoad)
                interceptorUpdate = false
            }
        }
    }

    private fun updateImage(image: ImageFile?, imageview: ImageView?) {
        val dir = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
        val file = image?.let {
            File(dir, "/${it.filepath}/${it.filename}/${it.index}")
        }
        file.let {
            Log.d("TAG-sun", "updateImage: ${it}")
            Glide.with(context)
                .load(it)
                .into(imageview!!)
        }
    }

    fun setOnBackListener(backListener: BackListener){
        this.backListener = backListener
    }

    interface BackListener {
        fun backFragment()
    }
}