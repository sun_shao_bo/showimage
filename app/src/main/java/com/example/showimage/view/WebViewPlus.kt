package com.example.showimage.view

import android.content.Context
import android.graphics.Rect
import android.text.InputType
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.inputmethod.BaseInputConnection
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputConnection
import android.webkit.WebView

class WebViewPlus @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : WebView(context, attrs, defStyleAttr) {

    private var scrollChanged:ScrollChanged? = null

    override fun onScrollChanged(l: Int, t: Int, oldl: Int, oldt: Int) {
        super.onScrollChanged(l, t, oldl, oldt)
        scrollChanged?.scrollChange()
    }

    fun setScrollChanged(scrollChanged: ScrollChanged) {
        this.scrollChanged = scrollChanged
    }

    interface ScrollChanged {
        fun scrollChange()
    }

    override fun onCreateInputConnection(outAttrs: EditorInfo?): InputConnection {
        val ic = BaseInputConnection(this, true)
        outAttrs!!.inputType =
            InputType.TYPE_CLASS_NUMBER
        return ic
    }

    override fun focusSearch(focused: View?, direction: Int): View {
        Log.d("TAG-sun", "focusSearch: ")
        return super.focusSearch(focused, direction)
    }

    override fun requestFocus(direction: Int, previouslyFocusedRect: Rect?): Boolean {
        Log.d("TAG-sun", "requestFocus: ")
        return super.requestFocus(direction, previouslyFocusedRect)
    }

    override fun findFocus(): View {
        Log.d("TAG-sun", "findFocus: ")
        return super.findFocus()
    }


}