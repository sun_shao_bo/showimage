package com.example.showimage.mvvm

import android.os.Handler
import android.os.Looper
import android.os.Message
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.showimage.base.Constant.HANDLER_CONTROLLER
import com.example.showimage.service.DownloadFileManager
import com.example.showimage.service.DownloadService

class WebViewModel:ViewModel() {

    private val controllerLiveData = MutableLiveData<Boolean>(true)

    private val urlLiveData = MutableLiveData<String>()

    private val urlList = mutableListOf<String>()

    private val showDialogLiveData = MutableLiveData<Boolean>(false)
    
    private val imageDownLoadVisibleLiveData = MutableLiveData<Boolean>(false)

    private val imageDownLoadProgressVisibleLiveData = MutableLiveData<String>()

    init {
        DownloadFileManager.getInstance().getBinder()?.setDownLoadListener(object : DownloadService.DownloadBinder.DownLoadListener{
            override fun onDownLoadListener(index: Int, size: Int, name: String) {
                imageDownLoadVisibleLiveData.postValue(true)
                imageDownLoadProgressVisibleLiveData.postValue("${index}/${size}")
                if (index == size){
                    imageDownLoadVisibleLiveData.postValue(false)
                }
            }
        })
    }

    fun getShowDialog():MutableLiveData<Boolean>{
        return showDialogLiveData
    }
    
    fun imageDownLoadVisibleLiveData():MutableLiveData<Boolean>{
        return imageDownLoadVisibleLiveData
    }

    fun imageDownLoadProgressVisibleLiveData():MutableLiveData<String>{
        return imageDownLoadProgressVisibleLiveData
    }
    fun setShowDialog(){
        Log.d("TAG-sun", "setShowDialog: ")
        if (urlList.size - 1 >= 0) {
            showDialogLiveData.value = true
        }else{
            showDialogLiveData.value = false
        }
    }

    fun getController():MutableLiveData<Boolean>{
        return controllerLiveData
    }

    fun setController(){
        h.removeCallbacksAndMessages(null)
        Message.obtain(h,0,true).sendToTarget()
        val message = Message.obtain(h,0,false)
        h.sendMessageDelayed(message, 10000)
    }

    fun setUrl(url:String){
        urlLiveData.value = url
        urlList.add(url)
    }

    fun loadPreUrl(){
        if (urlList.size > 1) {
            urlList.removeAt(urlList.size - 1)
        }
    }

    fun getUrlLiveData():MutableLiveData<String>{
        return urlLiveData
    }

    var h = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                HANDLER_CONTROLLER -> {
                    (msg.obj as Boolean).let {
                        controllerLiveData.value = it
                    }
                }
                1 -> {
//                    binding?.imageLoad?.visibility = View.VISIBLE
//                    binding?.imageLoad?.text = "${msg.arg1}/${msg.arg2}"
                }
            }
        }
    }
}