package com.example.showimage.mvvm

import android.app.Application
import androidx.lifecycle.ViewModel
import com.example.showimage.App
import com.example.showimage.db.AppDatabase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class ListViewModel : ViewModel(){

    private val nameListFlow = MutableStateFlow<List<String>>(emptyList())

    val _namelistFlow = nameListFlow.asStateFlow()

    suspend fun getNameList(){
        AppDatabase.getDatabase(App.instance)
            .imageFileDao()
            .getNames()
            .toMutableList().also {
                nameListFlow.value = it
            }
    }
}